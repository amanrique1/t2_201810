package test;
import junit.framework.TestCase;
import model.data_structures.DoubleLinkedList;
import model.data_structures.Lista;
import model.data_structures.Lista.Node;

public class PruebaLista <> extends TestCase{



	private DoubleLinkedList<String> personas;


	private void setupEscenario(){
		personas = new Lista<String>();
		personas.add("Daniel");
		personas.add("Andr�s");
		personas.add("Santiago");
		personas.add("Beatriz");
		personas.add("Juan");

	} 


	private void setupEscenario2(){
		personas = new Lista<String>();
		personas.add("Pedro");
	} 

	public void testAgregarObtener(){
		setupEscenario();
		assertEquals("el nombre no es el esperado", "Daniel", personas.get(3));
		assertEquals("el nombre no es el esperado", "Andr�s", personas.get(2));
		assertEquals("el nombre no es el esperado", "Santiago", personas.get(1));
		assertEquals("el nombre no es el esperado", "Beatriz", personas.get(4));	
		assertEquals("el nombre no es el esperado", "Juan", personas.get(0));

	}

	public void testTamanio(){
		setupEscenario();
		assertEquals(5, personas.size());
		personas.remove("Daniel");
		personas.remove("Juan");
		assertEquals(3, personas.size());
	}

	public void testBuscarUltimo(){
		setupEscenario();
		assertEquals("el nombre no es el esperado", "Juan", personas.last().getElement());
	}

	public void testEliminarPrimero2(){
		setupEscenario2();
		personas.delete("Pedro");
		assertNull(personas.get(0));
	}


}
