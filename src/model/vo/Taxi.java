package model.vo;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>{

	private String id;

	private String compania;
	public Taxi(String pId,String pCompania)
	{
		setId(pId);
		setCompania(pCompania);
	}
	public String getCompania() {
		return compania;
	}

	public void setCompania(String compania) {
		this.compania = compania;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
private Comparador sortKey;
	
	public void setSortKey(Comparador pSortKey) {
		sortKey = pSortKey;
	}


	@Override
	public int compareTo(Taxi otro) {
		// TODO Auto-generated method stub
		
		switch (sortKey) 
		{
		case TAXIID:

			return getId().compareTo(otro.getId());

		case COMPANIA:
			return getCompania().compareTo(otro.getCompania());
		}
		return -1;
	}	
}
