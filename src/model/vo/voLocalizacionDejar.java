package model.vo;

public class voLocalizacionDejar {
	
private String type;

private String coordinates[];

/**
 * @return the coordinates
 */
public String[] getCoordinates() {
	return coordinates;
}

/**
 * @param coordinates the coordinates to set
 */
public void setCoordinates(String coordinates[]) {
	this.coordinates = coordinates;
}

/**
 * @return the type
 */
public String getType() {
	return type;
}

/**
 * @param type the type to set
 */
public void setType(String type) {
	this.type = type;
}


}
