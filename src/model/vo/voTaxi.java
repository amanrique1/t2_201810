package model.vo;

public class voTaxi<T>
{
	private String id;

	private String compania;

	public voTaxi(String pId,String pCompania)
	{
		setId(pId);
		setCompania(pCompania);
	}

	public String getCompania() {
		return compania;
	}

	public void setCompania(String compania) {
		this.compania = compania;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
