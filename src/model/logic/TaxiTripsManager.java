package model.logic;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.Iterator;

import com.google.gson.Gson;

import api.ITaxiTripsManager;
import model.vo.Taxi;
import model.vo.voServicio;
import model.vo.voTaxi;
import model.vo.Comparador;
import model.vo.ObjectAdministrator;
import model.vo.Service;
import model.data_structures.DoubleLinkedList;
import model.data_structures.ILinkedList;
import model.data_structures.Node;

public class TaxiTripsManager<T extends Comparable<T>> implements ITaxiTripsManager {


	// TODO
	// Definition of data model 
	private DoubleLinkedList<Service> misCarreras;
	private DoubleLinkedList<Taxi> misTaxis;

	private boolean cargo=false;
	private static int depth = 0;
	public final static String archivo="./data/taxi-trips-wrvz-psew-subset-small.json";

	public void loadServices (String serviceFile) {
		// TODO Auto-generated method stub

		misCarreras= new DoubleLinkedList<Service>();
		misTaxis= new DoubleLinkedList<Taxi>();
		ArrayList<String>idTaxis=new ArrayList<>();
		Gson gson = new Gson();
		boolean existe;

		try
		{
			ObjectAdministrator[] myTypes = gson.fromJson(new FileReader(serviceFile), ObjectAdministrator[].class);
			Service actual;

			for (int i = 0; i < myTypes.length; i++) 
			{
				existe=false;
				misCarreras.add(myTypes[i].createService());
				for (int j = 0; j < idTaxis.size()&&!existe; j++) 
				{
					if(myTypes[i].getTaxi_id().equalsIgnoreCase(idTaxis.get(i)))
					{
						existe=true;
					}
				}
				if(existe==false)
				{
					misTaxis.add(myTypes[i].createTaxi());
				}



			}


		}
		catch (Exception e) 
		{
			System.out.println("*throws smoke bomb*");

		}
		misCarreras=sort(misCarreras);

		System.out.println("Inside loadServices with " + serviceFile);

	}

	public static <T extends Comparable<T>> DoubleLinkedList<T> sort(DoubleLinkedList<T> list) {
		depth++;
		String tabs = getTabs();

		System.out.println(tabs + "Sorting: " + list);

		if(list.size() <= 1) {
			depth--;
			return list;
		}

		DoubleLinkedList<T> left   = new DoubleLinkedList<T>();
		DoubleLinkedList<T> right  = new DoubleLinkedList<T>();
		DoubleLinkedList<T> result = new DoubleLinkedList<T>();

		int middle = list.size() / 2;

		int added = 0;
		T item;
		for (int i = 0; i < list.size(); i++) 
		{
			item=list.get(i).getElement();

			if(added++ < middle)
				left.add(item);
			else
				right.add(item);
		}

		left = sort(left);
		right = sort(right);

		result = merge(left, right);

		System.out.println(tabs + "Sorted to: " + result);

		depth--;
		return result;
	}

	/**
	 * Performs the oh-so-important merge step. Merges {@code left}
	 * and {@code right} into a new list, which is returned.
	 *
	 * @param left the left list
	 * @param right the right list
	 * @return a sorted version of the two lists' items
	 */
	private static <T extends Comparable<T>> DoubleLinkedList<T> merge(DoubleLinkedList<T> left,
			DoubleLinkedList<T> right) {
		String tabs = getTabs();
		System.out.println(tabs + "Merging: " + left + " & " + right);

		DoubleLinkedList<T> result = new DoubleLinkedList<T>();
		while(left.size() > 0 && right.size() > 0) {
			if(left.get(0).getElement().compareTo(right.get(0).getElement()) < 0)
				result.add(left.remove2(0));
			else
				result.add(right.remove2(0));
		}

		if(left.size() > 0)
		{
			for (int i = 0; i < left.size(); i++) {
				result.add(left.get(i));
			}


		}else
		{
			for (int i = 0; i < right.size(); i++) {
				result.add(right.get(i));
			}
		}

		return result;
	}

	private static String getTabs() {
		StringBuffer sb = new StringBuffer("");
		for(int i = 0; i < depth; i++)
			sb.append('\t');
		return sb.toString();
	}
	@Override
	public ILinkedList<Taxi> getTaxisOfCompany(String company) {
		// TODO Auto-generated method stub
		if(cargo==false)
		{
			loadServices(archivo);
		}
		ILinkedList<Taxi> compania= new DoubleLinkedList<>();
		Taxi nuevo;
		for (int i = 0; i < misTaxis.size(); i++) 
		{
			nuevo=misTaxis.get(i).getElement();
			if(nuevo.getCompania().equals(company))
			{
				compania.add(nuevo);
			}
		}


		return compania;
	}


	@Override
	public ILinkedList<Service> getTaxiServicesToCommunityArea(int communityArea) {
		// TODO Auto-generated method stub
		ILinkedList<Service> compania= new DoubleLinkedList<>();
		Service nuevo;
		for (int i = 0; i < misCarreras.size(); i++) 
		{
			nuevo=misCarreras.get(i).getElement();
			if(nuevo.getDropoff_community_area().equals(communityArea+""))
			{
				compania.add(nuevo);
			}
		}


		return compania;
	}

}
