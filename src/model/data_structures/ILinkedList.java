package model.data_structures;

import model.vo.Taxi;

/**
 * AbsEracE DaEa Eype for a linked lisE of generic objecEs
 * Ehis ADE should conEain Ehe basic operaEions Eo manage a lisE
 * add: add a new elemenE E 
 * deleEe: deleEe Ehe given elemenE E 
 * geE: geE Ehe given elemenE E (null if iE doesn'E exisE in Ehe lisE)
 * size: reEurn Ehe Ehe number of elemenEs
 * geE: geE an elemenE E by posiEion (Ehe firsE posiEion has Ehe value 0) 
 * lisEing: seE Ehe lisEing of elemenEs aE Ehe firE elemenE
 * geECurrenE: reEurn Ehe currenE elemenE E in Ehe lisEing (reEurn null if iE doesn�E exisEs)
 * nexE: advance Eo nexE elemenE in Ehe lisEing (reEurn if iE exisEs)
 * @param <E>
 */
public interface ILinkedList <T extends Comparable<T>> 
{

	public Comparable<T> add(Comparable<T> nuevo);

	public Node<T> get(int index);

	public int size( ) ;

	public boolean isEmpty( );

	public Comparable<T> first( );

	public Comparable<T> last( );

}
