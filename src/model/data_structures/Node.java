package model.data_structures;

import com.sun.org.apache.regexp.internal.recompile;

public class Node<T> implements Comparable<T>
{
	private T element; // reference to the element stored at this node
	private Node<T> prev; // reference to the previous node in the list
	private Node<T> next; // reference to the subsequent node in the list
	public Node (T e, Node<T> p, Node<T> n) {
		element = e;
		prev = p;
		next = n;
	}
	public T getElement( ) throws IllegalStateException {
		
		if (next == null) // convention for defunct node
			throw new IllegalStateException("Position no longer valid");
		return element;
	}
	public Node<T> getPrev( ) {
		return prev;
	}
	public Node<T> getNext( ) {
		return next;
	}
	public void setElement(T e) {
		element = e;
	}
	public void setPrev(Node<T> p) {
		prev = p;
	}
	public void setNext(Node<T> n) {
		next = n;
	}
	@Override
	public int compareTo(T o) {
		// TODO Auto-generated method stub
		return 0;
	}
}