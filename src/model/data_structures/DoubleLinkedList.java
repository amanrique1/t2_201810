package model.data_structures;

import java.util.Iterator;
import java.util.LinkedList;

import model.vo.Comparador;
import model.vo.Service;
import model.vo.Taxi;;

public class DoubleLinkedList <T extends Comparable<T>> implements ILinkedList<T>
{
	private Node<T> header; // header sentinel
	private Node<T> trailer; // trailer sentinel
	private int size = 0; // number of elements in the list

	public DoubleLinkedList( ) {
		header = new Node<>(null, null, null); // create header
		trailer = new Node<>(null, header, null); // trailer is preceded by header
		header.setNext(trailer); // header is followed by trailer
	}
	// private utilities

	private Node<T> validate(Comparable<T> p) throws IllegalArgumentException {
		if (!(p instanceof Node)) throw new IllegalArgumentException("Invalid p");
		Node<T> node = (Node<T>) p; // safe cast
		if (node.getNext( ) == null) // convention for defunct node
			throw new IllegalArgumentException("p is no longer in the list");
		return node;
	}


	private Comparable<T> position(Node<T> node) 
	{
		if (node == header || node == trailer)
			return null; // do not expose user to the sentinels
		return node;
	}

	public int size( ) 
	{ 
		return size; 
	}

	public boolean isEmpty( ) 
	{ 
		return size == 0; 
	}


	public Comparable<T> first( ) {
		return position(header.getNext( ));
	}

	public Comparable<T> last( ) {
		return position(trailer.getPrev( ));
	}
	public Comparable<T> before(Comparable<T> p) throws IllegalArgumentException {
		Node<T> node = validate(p);
		return position(node.getPrev( ));
	}

	public Comparable<T> after(Comparable<T> p) throws IllegalArgumentException {
		Node<T> node = validate(p);
		return position(node.getNext( ));
	}
	private Comparable<T> addBetween(T e, Node<T> pred, Node<T> succ) {
		Node<T> newest = new Node<>(e, pred, succ); // create and link a new node
		pred.setNext(newest);
		succ.setPrev(newest);
		size++;
		return newest;
	}

	public Comparable<T> addFirst(T e) {
		return addBetween(e, header, header.getNext( )); // just after the header
	}


	public Comparable<T> addLast(T e) {
		return addBetween(e, trailer.getPrev( ), trailer); // just before the trailer
	}

	public Comparable<T> addBefore(Comparable<T> p, T e)
			throws IllegalArgumentException {
		Node<T> node = validate(p);
		return addBetween(e, node.getPrev( ), node);
	}

	public Comparable<T> addAfter(Comparable<T> p, T e)
			throws IllegalArgumentException {
		Node<T> node = validate(p);
		return addBetween(e, node, node.getNext( ));
	}

	public T set(Comparable<T> p, T e) throws IllegalArgumentException {
		Node<T> node = validate(p);
		T answer = node.getElement( );
		node.setElement(e);
		return answer;
	}
	public T remove(Comparable<T> p) throws IllegalArgumentException {
		Node<T> node = validate(p);
		Node<T> predecessor = node.getPrev( );
		Node<T> successor = node.getNext( );
		predecessor.setNext(successor);
		successor.setPrev(predecessor);
		size--;
		T answer = node.getElement( );
		node.setElement(null); // help with garbage collection
		node.setNext(null); // and convention for defunct node
		node.setPrev(null);
		return answer;
	}
	public T remove2(int p) throws IllegalArgumentException {
		Node<T> node = get(p);
		Node<T> predecessor = node.getPrev( );
		Node<T> successor = node.getNext( );
		predecessor.setNext(successor);
		successor.setPrev(predecessor);
		size--;
		T answer = node.getElement( );
		node.setElement(null); // help with garbage collection
		node.setNext(null); // and convention for defunct node
		node.setPrev(null);
		return answer;
	}
	
	public Node<T> get(int index)
	{
		if(header == null || index > size -1)
		{
			return null;
		}
		if(index < 0 || index >= size)
		{
			System.out.println("This does not exist");
		}
		Node<T> p = header;
		int size = 0;
		while(size < index && p.getNext() != null)
		{
			p = p.getNext();
			size++;
		}
		if(size != index)
		{
			return null;
		}
		else
		{
			return p;
		}

	}

	@Override
	public Comparable add(Comparable e) {
		if(header==null)
		{
			header=new Node(e, null, null);
			return header;
		}
		else
		{
			return addBetween((T) e, trailer.getPrev( ), trailer); // just before the trailer
		}
	}
	
	public Comparable addSpecific(int pPosicion,Comparable e) {
		Node antes=get(pPosicion).getPrev();
		Node despues=get(pPosicion);
		return addBetween((T)e, antes, despues);
	}


}